﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class soundPlayer : MonoBehaviour {

	public GameObject soundButton;

	object[] mySounds;

	// Use this for initialization
	void Start () {

		mySounds = Resources.LoadAll ("SoundEffects");

		Debug.Log (mySounds.Length);
	
		GameObject myPanel = GameObject.Find ("Panel");

		for (int counter = 0; counter < mySounds.Length; counter++) {
			
			GameObject button = Instantiate (soundButton, myPanel.transform, false);

			button.GetComponentInChildren<Text> ().text = "Play Sound: " + counter;

			int index = counter;

			button.GetComponent<Button> ().onClick.AddListener (() => {
				playSound (index);
			});
		}

		GameObject button1 = Instantiate (soundButton, myPanel.transform, false);
		button1.GetComponentInChildren<Text> ().text = "Play random Sound ";
		Random.Range (playSound);
		
	}

	void playSound(int soundIndex){

		//sin0ce i know that all the elements in that array are audio clips
		AudioClip soundToPlay = (AudioClip)mySounds [soundIndex];

		Camera.main.GetComponent<AudioSource> ().PlayOneShot (soundToPlay);
	}
}

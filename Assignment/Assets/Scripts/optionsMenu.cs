﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class optionsMenu : MonoBehaviour {
	
	public Slider Volume;
	public AudioSource myMusic;

	//This is always checking the ammount of the slider to keep the volum up or down
	void Update()
	{
		if (Volume != null) {
			myMusic.volume = Volume.value;
		}
	}
	public void vibrate()
	{
		Handheld.Vibrate ();
		Debug.Log ("Vibrate Working");
	}
	//Pausing the music
	public void mute()
	{
		AudioListener.pause = !AudioListener.pause;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class dataSaving : MonoBehaviour {

	public List<users> allUsers;
	public int personToUpdate;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this);
		//this creates an empty new list
		allUsers = new List<users> ();

		//make sure there is only one data store at a time
		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (this.gameObject);
		}

	}

	public void saveToFile()
	{
		FileStream fs = new FileStream("newInfo.dat", FileMode.Create);

		// Construct a BinaryFormatter and use it to serialize the data to the stream.
		BinaryFormatter formatter = new BinaryFormatter();
		try 
		{
			formatter.Serialize(fs, allUsers);
		}
		catch (SerializationException e) 
		{
			Debug.Log("Failed to serialize. Reason: " + e.Message);
			throw;
		}
		finally 
		{
			fs.Close();
			Debug.Log ("Done");
		}
	}
	//This will load to the file 
	public void loadFromFile()
	{
		FileStream fs = new FileStream("newInfo.dat", FileMode.Open);
		try 
		{
			BinaryFormatter formatter = new BinaryFormatter();

			allUsers = (List<users>) formatter.Deserialize(fs);
		}
		catch (SerializationException e) 
		{
			Debug.Log("Failed to deserialize. Reason: " + e.Message);
			throw;
		}
		finally 
		{
			fs.Close();
		}
	}

}

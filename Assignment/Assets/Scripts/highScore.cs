﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highScore : MonoBehaviour {

	public static int score;
	public static int highscore;
	dataSaving saver;
	users personToUpdate;
	Text scoreText;

	void Start()
	{
		personToUpdate = new users();
		saver = GameObject.Find ("data").GetComponent<dataSaving> ();
		scoreText = GetComponent<Text> ();
		//call the load method in saver to get the scores in the hard disk
		dataSaving data = GameObject.Find ("data").GetComponent<dataSaving> ();
		data.loadFromFile ();
		//reload all scores
		score = 0;
	
		scoreText.text = highscore.ToString ();
	}

	void Update()
	{
		
		score = scoreScript.scoreAmmount;
		//Checking if score is greater than high score 
		if (score > highscore) {
			highscore = score;
			scoreText.text = "HighScore: " + score;
			users currentUser = new users ();//Saving the high score into the users
			currentUser.highScore = highscore;
			saver.allUsers.Add (currentUser);
		}

		//Saving the high score into the file
		if (GameObject.Find ("data") != null) {
			dataSaving datastore = GameObject.Find ("data").GetComponent<dataSaving> ();
			datastore.saveToFile ();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class star : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		//When colllider with that star score increases by 10 and both the star and needle are destoryed
		if (collider.gameObject.tag == "needleTag") {

			scoreScript.scoreAmmount += 10;
			Destroy (collider.gameObject);
			Destroy (this.gameObject);
		} 
	}
}

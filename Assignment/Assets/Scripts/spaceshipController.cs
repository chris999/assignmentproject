﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaceshipController : MonoBehaviour {

	public Vector2 speed = new Vector2(10, 10);
	float inputX;
	bool shoot;

	// Update is called once per frame
	void Update () {


		//Keyboard movments for the space ship
		Vector3 movement = new Vector3 (speed.x * inputX, 0, 0); 
		movement *= Time.deltaTime;
		transform.Translate (movement,Space.World);

		shoot = Input.GetKeyDown (KeyCode.Space);
		//This wont let the space ship go out of the scene
		Vector3 position = Camera.main.WorldToViewportPoint (transform.position);
		position.x = Mathf.Clamp01 (position.x);
		transform.position = Camera.main.ViewportToWorldPoint (position);

	}

	IEnumerator resetMovement()
	{
		yield return new WaitForSeconds (0.1f);
		inputX = 0;
	}
	//This will move to the left by -0.25f every press
	public void moveLeft()
	{
		inputX = -0.5f;
		StartCoroutine (resetMovement());
	}

	public void moveRight()
	{
		inputX = 0.5f;
		StartCoroutine (resetMovement());
	}
	//When shoot button is pressed it is calling the needle to spwan every time
	public void shootButton()
	{
		bool shoot = true;

		if (shoot) {
			spaceShipShooting weapon = GetComponent<spaceShipShooting> ();
			if (weapon != null) {
				weapon.attack (false);
			}
		}
	}


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaceShipShooting : MonoBehaviour {

	public Transform needle;

	public float shootRate = 0.10f;

	private float cooldown;
	// Use this for initialization
	void Start () {
		cooldown = 0f;//cooldown starts at 0
	}
	
	// Update is called once per frame
	void Update () {
		if (cooldown > 0) {
			cooldown -= Time.deltaTime;//Subtracting the cool down by the time.delta time
		}
	}

	public void attack(bool enemy)
	{
		cooldown = shootRate;//cooldwon is not 0.10f

		var shotTransform = Instantiate (needle) as Transform;//Calling the needle

		shotTransform.position = transform.position;
		//Needle moving up 
		needleController move = shotTransform.gameObject.GetComponent<needleController> ();
		if (move != null) {
			move.direction = Vector3.up;
		}
	}

	public bool attacking
	{
		get
		{ 
			return cooldown <= 0f;//Cool is back to 0
		}
	}
}

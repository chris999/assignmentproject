﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class bombScript : MonoBehaviour {

	//When the bomb is colided with the needle the game will end.
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "needleTag") {
			Destroy (collider.gameObject);
			SceneManager.LoadScene ("endScene");
		}
	}
}

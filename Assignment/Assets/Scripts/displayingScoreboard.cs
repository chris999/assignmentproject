﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class displayingScoreboard : MonoBehaviour {

	public GameObject personText;

	dataSaving dataStoring;

	// Use this for initialization
	void Start () {
		
		//Finding 'data' and getting it from datasaving
		if (GameObject.Find ("data") != null) {
			dataStoring = GameObject.Find ("data").GetComponent<dataSaving> ();

			refreshScores ();
			//calling these buttons to make as suppose with the use of their functions.
			GameObject.Find ("refreshButton").GetComponent<Button> ().onClick.AddListener (refreshScores);
			GameObject.Find ("backButton").GetComponent<Button> ().onClick.AddListener (backbuttonPressed);
		}
	}

	//This will refresh scores 
	void refreshScores(){
		//This will destoy the content which is there before the file was loaded
		foreach (Transform t in GameObject.Find("Content").transform) {
			Destroy (t.gameObject);
		}
		//This will call users from datastroing and creatsa text with the name, surname and highscore that has been created from datastroing
		foreach (users u in dataStoring.allUsers) {

			GameObject outtext = Instantiate (personText,
				GameObject.Find ("Content").transform, false);

			outtext.GetComponent<Text> ().text = "Name: " +" "+ u.name + " Surname: " +" "+ u.surname +
												" HighScore: " +" "+ u.highScore;	
		}
	}
	//Back button pressed gets you to the menu
	public void backbuttonPressed ()
	{
		SceneManager.LoadScene ("menu");
	}

}

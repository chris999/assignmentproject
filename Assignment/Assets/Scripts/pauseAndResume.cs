﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseAndResume : MonoBehaviour {

	public bool pause;

	// Use this for initialization
	void Start () {
		pause = false;
	}
	//When pause button is pressed the time scale is set to 1 which is the maximum so this will stop everything and after being again presed it continues
	public void onPause()
	{
		pause = !pause;
		if (!pause) {
			
			Time.timeScale = 1;
		}
		else if (pause) {
			
			Time.timeScale = 0;
		}
	}
}

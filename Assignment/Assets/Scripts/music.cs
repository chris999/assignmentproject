﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music : MonoBehaviour {

	static music instance = null;

	void Awake()
	{
		//As the game starts the music is set to start and doesnt destory on changing scenes
		if (instance != null) {
			Destroy (gameObject);
		} else {
			instance = this;
			GameObject.DontDestroyOnLoad (gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class loadScenes : MonoBehaviour {
    
	public InputField nameLog;
	public InputField surname;
	public TMP_Text invalid;
	dataSaving save;

	void Start()
	{
		//loading from file and getting the dataSaving from data
		save = GameObject.Find ("data").GetComponent<dataSaving> ();
		dataSaving data = GameObject.Find ("data").GetComponent<dataSaving> ();
		data.loadFromFile ();
	}
	//Scene loaders
	public void Play()
	{
		SceneManager.LoadScene ("Game");
	}

	public void Quit()
	{
		Application.Quit ();//Quiting the application when clikced
	}

	public void Score()
	{
		SceneManager.LoadScene ("scoreboard");
	}

	public void Help()
	{
		SceneManager.LoadScene ("helpScene");
	}

	public void Menu()
	{
		scoreScript.scoreAmmount = 0;//Reseting score
		SceneManager.LoadScene ("menu");
	}

	public void Restart()
	{
		scoreScript.scoreAmmount = 0;
		SceneManager.LoadScene ("Game");
	}

	public void login()
	{
		//This function will save into users the name and surname and checks if the user left the login deatils empty.
		users currentLog = new users ();
		if (currentLog.name == "" && currentLog.surname == " ") {
			invalid.text = "Invalid Input Login";
			save.allUsers.Add (currentLog);
		} else if (nameLog.text == "" && surname.text == "") {
			invalid.text = "Invalid Input Login";
		} else {
			logged ();
		}
	}
	public void logged()
	{
		SceneManager.LoadScene ("menu");
	}
}

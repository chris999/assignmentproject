﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class updatingScoreboard : MonoBehaviour {

	Button loginButton, backButton;
	dataSaving data;
	users personToUpdate;

	// Use this for initialization
	void Start () {
		personToUpdate = new users();
		loginButton = GameObject.Find ("loginButton").GetComponent<Button> ();//Calling the log in button
	
		loginButton.onClick.AddListener (loginButtonPressed);

		//Loading from file
		if (GameObject.Find ("data") != null) {
			dataSaving data = GameObject.Find ("data").GetComponent<dataSaving> ();
			data.loadFromFile ();
	}
	}
		
   void loginButtonPressed () {
		//Calling the objects from unity
		string nameLog = GameObject.Find ("nameField").GetComponent<InputField> ().text;
		string surname = GameObject.Find ("surnameField").GetComponent<InputField> ().text;
		//calling users and adding the objects to it.
		personToUpdate.name = nameLog;
		personToUpdate.surname = surname; 

		//Adding them to the file
		GameObject.Find ("data").GetComponent<dataSaving> ().allUsers.Add (personToUpdate);
		//Saving all into the file
		if (GameObject.Find ("data") != null) {
			dataSaving data = GameObject.Find ("data").GetComponent<dataSaving> ();
			data.saveToFile ();
		 }
		}
	}


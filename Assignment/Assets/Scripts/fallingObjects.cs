﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallingObjects : MonoBehaviour {
	
	float speed = 0.1f;

	void Start () {
		Destroy (gameObject, 4);//This destroyed the falling objects after 2 seconds
	}
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.down * speed * Time.deltaTime);//The speed in which the objects fall down
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ballonDestroy : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collider)
	{
		//if collided with needleTage score adds up to 2 and both the collider and the game object are destestroyed
		if (collider.gameObject.tag == "needleTag") {
			
			scoreScript.scoreAmmount += 2;
			Destroy(collider.gameObject);
			Destroy (this.gameObject);
		}	
	}

	void OnTriggerExit2D(Collider2D coll)
	{
		//if ballons hit the ground endScene is called and the game is over
		if (coll.gameObject.tag == "groundColl") {
			
			SceneManager.LoadScene ("endScene");
		}
	}
}

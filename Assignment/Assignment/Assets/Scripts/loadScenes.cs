﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadScenes : MonoBehaviour {
	bool log;

	void Start()
	{
		log = false;
	}

	public void Play()
	{
		SceneManager.LoadScene ("Game");
	}

	public void Quit()
	{
		Application.Quit ();
	}

	public void Score()
	{
		SceneManager.LoadScene ("scoreboard");
	}

	public void Menu()
	{
		scoreScript.scoreAmmount = 0;
		SceneManager.LoadScene ("menu");
	}

	public void Restart()
	{
		scoreScript.scoreAmmount = 0;
		SceneManager.LoadScene ("Game");
	}

	public void login()
	{
		if (log == true) {
			SceneManager.LoadScene ("menu");
		} else {
			SceneManager.LoadScene ("login");
		}
	}
}

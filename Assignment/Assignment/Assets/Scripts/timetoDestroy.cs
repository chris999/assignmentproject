﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timetoDestroy : MonoBehaviour {

	public int hitammount = 1;

	public bool objects = true;

	void OnTriggerEnter2D(Collider2D collider)
	{
		shooting shot = collider.gameObject.GetComponent<shooting> ();

		if (shot != null) {
			
			if (shot.enemyShoot != objects) {
				
				hitammount -= shot.dmg;
				Destroy (shot.gameObject);

				if (hitammount <= 0) {
					Destroy (gameObject);
				}
			}
		}
	}
}

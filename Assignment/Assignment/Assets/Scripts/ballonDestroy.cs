﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ballonDestroy : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "needleTag") {
			
			scoreScript.scoreAmmount += 2;
			Destroy(collider.gameObject);
			Destroy (this.gameObject);
		}	

	}

	void OnTriggerExit2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "groundColl") {
			
			SceneManager.LoadScene ("endScene");
		}
	}
}

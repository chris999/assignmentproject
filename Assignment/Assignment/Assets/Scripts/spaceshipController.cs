﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spaceshipController : MonoBehaviour {

	public Vector2 speed = new Vector2(10, 10);

	// Update is called once per frame
	void Update () {

		float inputX = Input.GetAxisRaw ("Horizontal");
	
		Vector3 movement = new Vector3 (speed.x * inputX, 0, 0); 
		movement *= Time.deltaTime;
		transform.Translate (movement,Space.World);

		bool shoot = Input.GetKeyDown (KeyCode.Space);

		if (shoot) {
			spaceShipShooting weapon = GetComponent<spaceShipShooting> ();
			if (weapon != null) {
				weapon.attack (false);
			}
		}
			
		Vector3 position = Camera.main.WorldToViewportPoint (transform.position);
		position.x = Mathf.Clamp01 (position.x);
		transform.position = Camera.main.ViewportToWorldPoint (position);

	}
}

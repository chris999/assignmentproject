﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallingObjects : MonoBehaviour {
	
	float speed = 2;

	void Start () {
		Destroy (gameObject, 2);
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.down * speed * Time.deltaTime);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class star : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "star") {

			scoreScript.scoreAmmount += 10;
			Destroy (collider.gameObject);
			Destroy (this.gameObject);
		}
	}
}

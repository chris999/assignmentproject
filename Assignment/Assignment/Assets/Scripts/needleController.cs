﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class needleController : MonoBehaviour {

	public Vector2 speed = new Vector2 (10, 10);

	public Vector2 direction = new Vector2 (0, 1);

	void Start()
	{
		Destroy (gameObject, 1);
	}

	void Update()
	{
		Vector3 movement = new Vector3 (speed.x * direction.x, speed.y * direction.y, 0);

		movement *= Time.deltaTime;
		transform.Translate (movement,Space.World);
	}
}
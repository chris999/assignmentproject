﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonControll : MonoBehaviour {

	public float buttonSpeed = 10;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
	}

	void OnMouseDown(){
		if (Input.GetMouseButton (0)) {
			transform.Translate (Vector2.right * buttonSpeed * Time.deltaTime);
		}
	}
}

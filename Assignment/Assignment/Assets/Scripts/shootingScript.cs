using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootingScript : MonoBehaviour {

	public Transform needle;

	public float shootRate = 0.20f;

	private float cooldown;
	// Use this for initialization
	void Start () {
		cooldown = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (cooldown > 0) {
			cooldown -= Time.deltaTime;
		}
	}

	public void attack(bool enemy)
	{
		cooldown = shootRate;

		var shotTransform = Instantiate (needle) as Transform;

		shotTransform.position = transform.position;

		shooting shot = shotTransform.gameObject.GetComponent<shooting> ();
		if (shot != null) {
			shot.enemyShoot = enemy;
		}

		needleController move = shotTransform.gameObject.GetComponent<needleController> ();
		if (move != null) {
			move.direction = Vector3.up;
		}
	}

	public bool attacking
	{
		get
		{ 
			return cooldown <= 0f;
		}
	}
}

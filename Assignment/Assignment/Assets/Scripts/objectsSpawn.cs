﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectsSpawn : MonoBehaviour {

	public GameObject[] Objects;
	public Vector3 value;
	public float spawnWait;
	public float spawnMostWait;
	public float spawnLeastWait;
	public int start;
	public bool stop;

	int randEnemy;

	// Use this for initialization
	void Start () {
		StartCoroutine (waitSpawn ());
	}
	
	// Update is called once per frame
	void Update () {
		spawnWait = Random.Range (spawnLeastWait, spawnMostWait);
	}

	IEnumerator waitSpawn()
	{
		yield return new WaitForSeconds (spawnWait);
		while (!stop) {
			randEnemy = Random.Range (0, 6);
			Vector3 spawnPosition = new Vector3 (Random.Range (-value.x, value.x), 1, Random.Range (-value.y, value.y));
			Instantiate(Objects[randEnemy], spawnPosition + transform.TransformPoint(0, 0, 0),transform.rotation);
			yield return new WaitForSeconds (spawnWait);
		}
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class optionsMenu : MonoBehaviour {

	public Slider Volume;
	public AudioSource myMusic;

	// Use this for initialization
	void Start () {
		myMusic.volume = Volume.value;
	}

	public void vibrate()
	{
		Handheld.Vibrate ();
		Debug.Log ("Vibrate Working");
	}

	public void mute()
	{
		AudioListener.pause = !AudioListener.pause;
	}
}

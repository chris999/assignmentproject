﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class highScore : MonoBehaviour {

	public static int score;

	public static int highscore;

	Text scoreText;

	void Start()
	{
		scoreText = GetComponent<Text> ();

		score = 0;
		highscore = PlayerPrefs.GetInt ("highscore", highscore);
		scoreText.text = highscore.ToString ();
	}

	void Update()
	{
		score = scoreScript.scoreAmmount;
		if (score > highscore) {
			highscore = score;
			scoreText.text = "HighScore: " + score;

			PlayerPrefs.SetInt ("highscore", highscore);
		}
	}
}
